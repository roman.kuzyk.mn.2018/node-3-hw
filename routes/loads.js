const express = require('express');
const router = new express.Router();
const Load = require('../models/load');
const middleware = require('../middlewares');
const verifyToken = middleware.verify;
const Driver = require('../models/driver');
const Truck = require('../models/truck');

const availbleStates = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];


router.post('/loads', verifyToken, (req, res) => {
  const userId = req.user.data._id;
  const userRole = req.user.data.role;
  if (userRole === 'SHIPPER') {
    if (req.body.name && req.body.payload && req.body.dimensions &&
       req.body.pickup_address && req.body.delivery_address) {
      const newPost = new Load({
        created_by: userId,
        name: req.body.name,
        status: 'NEW',
        dimensions: req.body.dimensions,
        payload: req.body.payload,
        pickup_address: req.body.pickup_address,
        delivery_address: req.body.delivery_address,
        logs: {
          message: 'Load was created',
          time: Date.now().toString(),
        },
      });
      newPost.save()
          .then(() => {
            res.status(200).json({
              message: 'All done',
              created_by: userId,
              name: req.body.name,
              status: 'NEW',
              dimensions: req.body.dimensions,
              payload: req.body.payload,
              pickup_address: req.body.pickup_address,
              delivery_address: req.body.delivery_address,
              Date: Date.now(),
            });
          })
          .catch((error) => {
            res.status(500).json(error);
          });
    } else {
      res.status(400).json({message: 'Invalid body input'});
    }
  } else {
    res.status(400).json({message: 'Invalid role'});
  }
});

router.get('/loads', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  if (userRole === 'SHIPPER') {
    let limit = +req.query.limit;
    let offset = +req.query.offset;
    const status = req.query.status;
    console.log('status', status);

    if (!offset) offset = 0;
    if (!limit) limit = 0;
    const userId = req.user.data._id;
    if (status) {
      Load.find({created_by: userId, status: status}).skip(offset).limit(limit)
          .then((loads) => {
            console.log('test truck', loads);
            if (loads) {
              res.status(200).json({loads});
              console.log('truck', loads);
              next();
            }
          })
          .catch((error) => {
            res.status(500).json(error);
          });
    } else {
      Load.find({created_by: userId}).skip(offset).limit(limit)
          .then((loads) => {
            console.log('test truck', loads);
            if (loads) {
              res.status(200).json({loads});
              console.log('truck', loads);
              next();
            }
          })
          .catch((error) => {
            res.status(500).json(error);
          });
    }
  } else {
    res.status(400).json({message: 'You dont have permision to do that'});
  }
});

router.get('/loads/active', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  const userId = req.user.data._id;
  if (userRole === 'DRIVER') {
    Driver.findOne({_id: userId}).then((user) => {
      if (user) {
        console.log('works');

        const assignedLoad = user.assignedLoad;
        console.log('assignedLoad', assignedLoad);

        if (assignedLoad) {
          Load.findOne({_id: assignedLoad})
              .then((load) => {
                if (load) {
                  res.status(200).json({load});
                  console.log('load', load);
                } else {
                  res.status(400).json({error: 'Wrong load id'});
                }
              })
              .catch((error) => {
                res.status(500).json({error, message: 'bliad'});
              });
        } else {
          res.status(400).json({message: 'You dont have assigned load sir'});
        }
      } else {
        res.status(400).json({message: 'Incorect request'});
      }
    });
  } else {
    res.status(400).json({message: 'You dont have permision to do that'});
  }
});
router.get('/loads/:id/shipping_info', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  if (userRole === 'SHIPPER') {
    const loadId = req.params.id;
    Load.findOne({_id: loadId})
        .then((load) => {
          if (load) {
            const assigned = load.assigned_to;
            console.log('assigned', assigned);
            if (assigned && load.status === 'ASSIGNED') {
              Driver.findOne({_id: assigned}).then((user) => {
                if (user.assignedTruck) {
                  Truck.findOne({_id: user.assignedTruck}).then((truck) => {
                    res.status(200).json({load, truck});
                    console.log('load', load);
                    next();
                  });
                }
              });
            } else {
              res.status(400)
                  .json({
                    message: 'Your load is not assigned to any drivers ',
                  });
            }
          } else {
            res.status(400).json({message: 'Wrong load id'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  } else {
    res.status(400).json({
      message: 'You dont have permision to do that sucker'});
  }
});


router.get('/loads/:id', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  if (userRole === 'SHIPPER') {
    const loadId = req.params.id;
    Load.findOne({_id: loadId})
        .then((load) => {
          if (load) {
            res.status(200).json({load});
            console.log('load', load);
            next();
          } else {
            res.status(400).json({error: 'Wrong load id'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  } else {
    res.status(400).json({
      message: 'You dont have permision to do that sucker'});
  }
});

router.post('/loads/:id/post', verifyToken, (req, res) => {
  let logs = [];
  const userRole = req.user.data.role;
  const userId = req.user.data._id;
  if (userRole === 'SHIPPER') {
    const loadId = req.params.id;
    Load.findOne({_id: loadId})
        .then((load) => {
          if (load) {
            if (load.created_by === userId) {
              if (load.status === 'NEW') {
                logs = load.logs;
                console.log('logs', logs);

                const loadWidth = load.dimensions.width;
                const loadLength = load.dimensions.length;
                const loadHeight = load.dimensions.height;
                const loadPayload = load.payload;
                logs.push({message: 'Status was changed to POSTED',
                  Date: Date.now().toString()});
                Load.updateOne({_id: loadId},
                    {status: 'POSTED', logs: logs}).then(() => {
                  Truck.findOne({
                    assigned_to: {$exists: true},
                    status: 'IS',
                    length: {$gt: loadLength},
                    height: {$gt: loadHeight},
                    width: {$gt: loadWidth},
                    payload: {$gt: loadPayload},
                  }).then((truck) => {
                    if (truck) {
                      const driverOwner = truck.assigned_to;
                      const truckId = truck._id;
                      Driver.findOne({_id: driverOwner}).then((driver) => {
                        if (driver) {
                          Driver.updateOne({_id: driverOwner},
                              {assignedLoad: loadId}).then(() => {
                            Truck.updateOne({_id: truckId},
                                {status: 'OL'}).then(() => {
                              logs.push({
                                message: 'Status was changed to ASSIGNED',
                                Date: Date.now().toString()});
                              Load.updateOne({_id: loadId}, {
                                status: 'ASSIGNED', assigned_to: driverOwner,
                                state: availbleStates[0], logs: logs,
                              }).then(() => {
                                res.status(200).json({
                                  message: 'Load posted successfully',
                                  driver_found: true,
                                });
                              });
                            });
                          });
                        } else {
                          Load.updateOne({_id: loadId},
                              {status: 'NEW'}).then(() => {
                            Truck.updateOne({assigned_to: {$exists: true},
                              status: 'IS'},
                            {$unset: {assigned_to: driverOwner}})
                                .then(() => {
                                  res.status(500)
                                      .json({message: 'Please try again'});
                                });
                          });
                        }
                      });
                    } else {
                      logs.push({message: 'Status was changed to NEW',
                        Date: Date.now().toString()});
                      Load.updateOne({_id: loadId},
                          {status: 'NEW'}).then(() => {
                        res.status(500).json({
                          message: 'We didnt find truck for you',
                        });
                      });
                    }
                  });
                });
              } else {
                res.status(400)
                    .json({message: 'You cant post this load anymore'});
              }
            } else {
              res.status(400)
                  .json({message: 'You dont have premision to do that'});
            }
          } else {
            res.status(400).json({error: 'Wrong load id'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  } else {
    res.status(400).json({message: 'You dont have permision to do that'});
  }
});

router.patch('/loads/active/state', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  const userId = req.user.data._id;
  let logs = [];
  if (userRole === 'DRIVER') {
    Driver.findOne({_id: userId}).then((user) => {
      if (user) {
        const assignedLoad = user.assignedLoad;
        if (assignedLoad) {
          Load.findOne({_id: assignedLoad})
              .then((load) => {
                if (load) {
                  logs = load.logs;
                  let currentState = availbleStates
                      .findIndex((e) => load.state === e);
                  if (currentState < 3) {
                    currentState = currentState + 1;
                    if (availbleStates[currentState]=== 'Arrived to delivery') {
                      logs.push({
                        message: `Load state changed to ${availbleStates[currentState]} and status to 'SHIPPED'`,
                        time: Date.now().toString(),
                      });
                      Truck.updateOne({assigned_to: userId},
                          {status: 'IS'},
                          {$unset: {assignedLoad: assignedLoad}})
                          .then(() => {
                            Load.updateOne({_id: assignedLoad},
                                {state: availbleStates[currentState],
                                  status: 'SHIPPED', logs: logs}).then(() => {
                              Driver.updateOne({_id: userId},
                                  {$unset:
                                     {assignedLoad: assignedLoad}}).then(() => {
                                res.status(200)
                                    .json({
                                      message: `Successfully delivered your load ${load.name}`});
                              });
                            });
                          });
                    } else {
                      logs.push({
                        message: `Load state changed to ${availbleStates[currentState]}`,
                        time: Date.now().toString(),
                      });
                      Load.updateOne({_id: assignedLoad},
                          {state: availbleStates[currentState], logs: logs})
                          .then(() => {
                            res.status(200)
                                .json({message: `Successfully changed state to ${availbleStates[currentState]}`});
                          });
                    }
                  } else {
                    res.status(400).json({message: 'Load is already delivere'});
                  }
                } else {
                  res.status(400).json({error: 'Wrong load id'});
                }
              })
              .catch((error) => {
                res.status(500).json(error);
              });
        } else {
          res.status(400).json({message: 'You dont have assigned load sir'});
        }
      }
    });
  } else {
    res.status(400).json({message: 'You dont have permision to do that'});
  }
});

router.delete('/loads/:id', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  const loadId = req.params.id;
  const userId = req.user.data._id;
  if (userRole === 'SHIPPER') {
    Load.findOne({_id: loadId}).then((load) => {
      if (load) {
        if (load.created_by === userId) {
          if (load.status === 'NEW') {
            Load.remove({_id: loadId}).then(() => {
              res.status(200).json({message: `${loadId} deleted successfully`});
              next();
            });
          } else {
            res.status(400)
                .json({message: 'You can delete load with "NEW" status only'});
          }
        } else {
          res.status(400).json({message: 'You can delete only your loads'});
        }
      } else {
        res.status(400).json({message: 'Invalid load id'});
      }
    });
  } else {
    res.status(400).json({message: 'Invalid role'});
  }
});

router.put('/loads/:id', verifyToken, (req, res) => {
  const userId = req.user.data._id;
  const userRole = req.user.data.role;
  if (userRole === 'SHIPPER') {
    if (req.body.name && req.body.payload && req.body.dimensions &&
      req.body.pickup_address && req.body.delivery_address) {
      const loadId = req.params.id;
      Load.findOne({_id: loadId}).then((load) => {
        if (load) {
          const logs = load.logs;
          if (load.status === 'NEW') {
            logs.push({
              message: `Load Data was updated `,
              time: Date.now().toString(),
            });
            Load.updateOne({_id: loadId}, {
              name: req.body.name,
              payload: req.body.payload,
              pickup_address: req.body.pickup_address,
              delivery_address: req.body.delivery_address,
              status: 'NEW',
              dimensions: req.body.dimensions,
              logs: logs,
            }).then(() => {
              res.status(200).json({
                created_by: userId,
                name: req.body.name,
                status: 'NEW',
                dimensions: req.body.dimensions,
                payload: req.body.payload,
                pickup_address: req.body.pickup_address,
                delivery_address: req.body.delivery_address,
                Date: Date.now(),
              });
            })
                .catch((error) => {
                  res.status(500).json(error);
                });
          } else {
            res.status(400)
                .json({message: 'You can update only loads with "NEW" status'});
          }
        } else {
          res.status(400).json({message: 'No load with this ID found'});
        }
      });
    } else {
      res.status(400).json({message: 'Invalid body input'});
    }
  } else {
    res.status(400).json({message: 'Invalid role'});
  }
});

module.exports = router;
