const express = require('express');
const router = new express.Router();
const Load = require('../models/load');
const middleware = require('../middlewares');
const verifyToken = middleware.verify;


router.get('/posts', verifyToken, (req, res, next) => {
  let limit = +req.query.limit;
  let offset = +req.query.offset;
  console.log('type', typeof (limit));
  if (!offset) offset = 0;
  if (!limit) limit = 0;
  const userId = req.user.data._id;
  Load.find({id: userId}).skip(offset).limit(limit)
      .then((post) => {
        console.log('test post', post);
        if (post) {
          res.status(200).json({post});
          console.log('post', post);
          next();
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});

router.post('/posts', verifyToken, (req, res, next) => {
  const userId = req.user.data._id;
  console.log('req.user', req.user.data._id);
  console.log('userId ', userId);
  console.log('req.body.text', req.body.text);
  const newPost = new Load({id: userId, text: req.body.text, checked: false});
  newPost.save()
      .then(() => {
        res.status(200).json({
          text: req.body.text,
          answer: 'success',
        });
        next();
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});

router.get('/posts/:id', verifyToken, (req, res, next) => {
  const test = req.params.id;
  Load.findOne({_id: test})
      .then((post) => {
        if (post) {
          res.status(200).json({post});
          console.log('post', post);
          next();
        } else {
          res.status(400).json({error: 'Wrong post id'});
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});
router.patch('/posts/:id', verifyToken, (req, res, next) => {
  const postId = req.params.id;
  Load.findOne({_id: postId})
      .then((post) => {
        if (post) {
          post.checked = !post.checked;
          Load.updateOne({_id: postId}, {$set: {checked: post.checked}})
              .then(() => {
                res.status(200).json({post});
                next();
              });
        } else {
          res.status(400).json({error: 'Wrong post id'});
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});

router.delete('/posts/:id', verifyToken, (req, res, next) => {
  const postId = req.params.id;
  Load.findOne({_id: postId}).then((post) => {
    if (post) {
      Load.remove({_id: postId}).then(() => {
        res.status(200).json({message: `${postId} deleted successfully`});
        next();
      });
    } else {
      res.status(400).json({message: 'Invalid post id'});
    }
  });
});

module.exports = router;
