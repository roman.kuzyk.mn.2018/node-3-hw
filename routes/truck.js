const express = require('express');
const router = new express.Router();
const Truck = require('../models/truck');
const middleware = require('../middlewares');
const verifyToken = middleware.verify;
const Driver = require('../models/driver');
allowedTypes = [
  {
    type: 'SPRINTER',
    length: 300,
    width: 250,
    height: 170,
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    length: 500,
    width: 250,
    height: 170,
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    length: 700,
    width: 350,
    height: 200,
    payload: 4000,
  },
];

// !add truck to truck arr to Driver in mongo
router.post('/trucks', verifyToken, (req, res) => {
  const userRole = req.user.data.role;
  if (userRole === 'DRIVER') {
    const truck = req.body.type;
    const isAllowed = allowedTypes
        .find((isAllowed) => isAllowed.type === truck);
    if (isAllowed) {
      console.log('isAllowed', isAllowed);
      const userId = req.user.data._id;
      console.log('req.user', req.user.data._id);
      console.log('userId ', userId);

      const newPost = new Truck({
        created_by: userId,
        type: req.body.type,
        status: 'IS',
        length: isAllowed.length,
        width: isAllowed.width,
        height: isAllowed.height,
        payload: isAllowed.payload,
      });
      newPost.save()
          .then(() => {
            res.status(200).json({
              created_by: userId,

              type: isAllowed.type,
              status: 'IS',
              length: isAllowed.length,
              width: isAllowed.width,
              height: isAllowed.height,
              payload: isAllowed.payload,
              message: 'success',
              Date: Date.now(),
            });
          })
          .catch((error) => {
            res.status(500).json(error);
          });
    } else {
      res.status(400).json({message: 'Invalid truck type'});
    }
  } else {
    res.status(400).json({message: 'Invalid role'});
  }
});

router.put('/trucks/:id', verifyToken, (req, res) => {
  const userRole = req.user.data.role;
  if (userRole === 'DRIVER') {
    if (req.body.type) {
      const isAllowed = allowedTypes
          .find((isAllowed) => isAllowed.type === req.body.type);
      const truckId = req.params.id;
      if (isAllowed) {
        Truck.findOne({_id: truckId}).then((truck) => {
          if (truck) {
            if (truck.status === 'IS') {
              Truck.updateOne({_id: truckId}, {
                type: req.body.type,
                status: 'IS',
                length: isAllowed.length,
                width: isAllowed.width,
                height: isAllowed.height,
                payload: isAllowed.payload,
              }).then(() => {
                res.status(200).json({
                  type: isAllowed.type,
                  status: 'IS',
                  length: isAllowed.length,
                  width: isAllowed.width,
                  height: isAllowed.height,
                  payload: isAllowed.payload,
                  message: 'success',
                });
              })
                  .catch((error) => {
                    res.status(500).json(error);
                  });
            } else {
              res.status(400)
                  .json({message: 'You can update truck only with "IS" statu'});
            }
          } else {
            res.status(400).json({message: 'No truck with this ID found'});
          }
        });
      } else {
        res.status(400).json({message: 'Invalid truck type'});
      }
    } else {
      res.status(400).json({message: 'Invalid body input'});
    }
  } else {
    res.status(400).json({message: 'Invalid role'});
  }
});

router.delete('/trucks/:id', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  const truckId = req.params.id;
  const userId = req.user.data._id;
  if (userRole === 'DRIVER') {
    Truck.findOne({_id: truckId}).then((truck) => {
      if (truck) {
        if (truck.created_by === userId) {
          if (truck.status === 'IS') {
            Truck.remove({_id: truckId}).then(() => {
              res.status(200)
                  .json({message: `${truckId} deleted successfully`});
              next();
            });
          } else {
            res.status(400)
                .json({message: 'You can delete truck with "IS" status only'});
          }
        } else {
          res.status(400).json({message: 'You can delete only your trucks'});
        }
      } else {
        res.status(400).json({message: 'Invalid load id'});
      }
    });
  } else {
    res.status(400).json({message: 'Invalid role'});
  }
});


router.get('/trucks', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  if (userRole === 'DRIVER') {
    let limit = +req.query.limit;
    let offset = +req.query.offset;

    if (!offset) offset = 0;
    if (!limit) limit = 0;
    const userId = req.user.data._id;
    Truck.find({created_by: userId}).skip(offset).limit(limit)
        .then((trucks) => {
          console.log('test truck', trucks);
          if (trucks) {
            res.status(200).json({trucks});
            console.log('truck', trucks);
            next();
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  }
});

router.get('/trucks/:id', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  if (userRole === 'DRIVER') {
    const truckId = req.params.id;
    Truck.findOne({_id: truckId})
        .then((truck) => {
          if (truck) {
            res.status(200).json({truck});
            console.log('truck', truck);
            next();
          } else {
            res.status(400).json({error: 'Wrong truck id'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  }
});

router.post('/trucks/:id/assign', verifyToken, (req, res, next) => {
  const userRole = req.user.data.role;
  if (userRole === 'DRIVER') {
    const userId = req.user.data._id;
    console.log('id', userId);
    const truckId = req.params.id;
    Truck.findOne({
      _id: truckId,
    })
        .then((truck) => {
          if (truck) {
            const truckStatus = truck.status;
            console.log('truckStatus', truckStatus);
            if (truckStatus === 'IS') {
              Driver.findOne({_id: userId}).then((user) => {
                const isBusy = user.assignedLoad;
                console.log('isbus', isBusy);
                if (!isBusy) {
                  if (user.assignedTruck) {
                    console.log('if truck', user.assignedTruck);
                    Truck.findOne({_id: user.assignedTruck}).then((truck) => {
                      if (truck) {
                        console.log('truck', truck);
                        console.log('user.assignedTruck', user.assignedTruck);
                        Truck.updateOne({_id: user.assignedTruck},
                            {$unset: {assignedTo: userId}}).then(() => {
                          Truck.updateOne({_id: truckId}, {
                            assigned_to: userId,
                          }).then(() => {
                            Driver.findOne({_id: userId}).then((user) => {
                              console.log('works');
                              console.log('user', user);
                              if (user) {
                                Driver.updateOne({_id: userId}, {
                                  assignedTruck: truckId,
                                }).then(() => {
                                  res.status(200)
                                      .json({message: 'success', truck});
                                  next();
                                });
                              }
                            });
                          });
                        });
                      }
                    });
                  } else {
                    Truck.updateOne({_id: truckId}, {
                      assigned_to: userId,
                    }).then(() => {
                      Driver.findOne({_id: userId}).then((user) => {
                        console.log('works');
                        console.log('user', user);
                        if (user) {
                          Driver.updateOne({_id: userId}, {
                            assignedTruck: truckId,
                          }).then(() => {
                            res.status(200).json({message: 'success', truck});
                            next();
                          });
                        }
                      });
                    });
                  }
                } else {
                  res.status(400)
                      .json({
                        message: 'Sir you cant change assigned truck'});
                }
              });
            } else {
              res.status(400)
                  .json({message: 'You cant assign  truck with OL status'});
            }
          } else {
            res.status(400).json({message: 'Wrong truck id'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  }
});


module.exports = router;
