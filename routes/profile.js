const express = require('express');
const router = new express.Router();
const rounds = 10;
const middleware = require('../middlewares');
const verifyToken = middleware.verify;
const Driver = require('../models/driver');
const Shipper = require('../models/shipper');
const bcrypt = require('bcrypt');


router.get('/users/me', verifyToken, (req, res, next) => {
  const userId = req.user.data._id;
  const userRole = req.user.data.role;
  console.log('userRole', userRole);

  if (userRole === 'DRIVER') {
    Driver.findOne({_id: userId})
        .then((post) => {
          console.log('post ', post);
          if (post) {
            const user = {
              _id: userId,
              email: post.email,
              role: post.role,
              trucks: post.trucks,
              assignedTrucks: post.assignedTrucks,
              assignedLoad: post.assignedLoad,
            };
            res.status(200).json({user});
            console.log('post', user);
            next();
          } else {
            res.status(400).json({message: 'User not found'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  } else {
    Shipper.findOne({_id: userId})
        .then((post) => {
          console.log('post ', post);
          if (post) {
          // !update user loads
            const userInfo = {
              _id: userId,
              email: post.email,
              role: post.role,
              loads: post.loads,
            };
            res.status(200).json({userInfo});
            console.log('post', userInfo);
            next();
          } else {
            res.status(400).json({message: 'User not found'});
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  }
  console.log('userRole', userRole);
});

router.delete('/users/me', verifyToken, (req, res, next) => {
  const userId = req.user.data._id;
  const userRole = req.user.data.role;
  console.log('userRole', userRole);

  if (userRole === 'DRIVER') {
    Driver.findOne({_id: userId}).then((post) => {
      if (post) {
        Driver.remove({_id: userId}).then(() => {
          res.status(200).json({message: `${userId} deleted successfully`});
          next();
        });
      } else {
        res.status(400).json({message: 'Invalid user id'});
      }
    });
  } else {
    Shipper.findOne({_id: userId}).then((post) => {
      if (post) {
        Shipper.remove({_id: userId}).then(() => {
          res.status(200).json({message: `${userId} deleted successfully`});
          next();
        });
      } else {
        res.status(400).json({message: 'Invalid user id'});
      }
    });
  }
});

router.patch('/users/me/password', verifyToken, (req, res) => {
  const userId = req.user.data._id;
  const userRole = req.user.data.role;
  console.log('userId', userId);
  if (userRole === 'SHIPPER') {
    Shipper.findOne({_id: userId}).then((user) => {
      if (user) {
        bcrypt.compare(req.body.oldPassword, user.password, (error, match) => {
          console.log(match);

          if (error) res.status(500).json(error);
          else if (match) {
            bcrypt.hash(req.body.newPassword, rounds, (error, hash) => {
              if (error) res.status(500).json(error);
              else {
                Shipper.updateOne({_id: userId}, {password: hash}).then(() => {
                  res.status(200)
                      .json({message: 'Password was changed successfully'});
                });
              }
            });
          } else res.status(400).json({error: 'passwords do not match'});
        });
      }
    });
  } else {
    Driver.findOne({_id: userId}).then((user) => {
      if (user) {
        bcrypt.compare(req.body.oldPassword, user.password, (error, match) => {
          console.log(match);

          if (error) res.status(500).json(error);
          else if (match) {
            bcrypt.hash(req.body.newPassword, rounds, (error, hash) => {
              console.log('hash', hash);

              if (error) res.status(500).json(error);
              else {
                Driver.updateOne({_id: userId}, {password: hash}).then(() => {
                  res.status(200)
                      .json({message: 'Password was changed successfully'});
                });
              }
            });
          } else res.status(400).json({error: 'passwords do not match'});
        });
      }
    });
  }
});


module.exports = router;
