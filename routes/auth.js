const express = require('express');
const router = new express.Router();
const Driver = require('../models/driver');
const Shipper = require('../models/shipper');
const bcrypt = require('bcrypt');
const rounds = 10;

const jwt = require('jsonwebtoken');
const tokenSecret = 'my-token-secret';

const middleware = require('../middlewares');


router.post('/login', (req, res) => {
  Shipper.findOne({email: req.body.email}).then((user) => {
    if (user) {
      bcrypt.compare(req.body.password, user.password, (error, match) => {
        console.log(match);

        if (error) res.status(500).json(error);
        else if (match) {
          res.status(200).json({
            message: 'success',
            jwt_token: generateToken(user),
          });
        } else res.status(400).json({error: 'passwords do not match'});
      });
    } else {
      Driver.findOne({email: req.body.email})
          .then((user) => {
            if (!user) {
              res.status(400)
                  .json({error: 'no user with that email found'});
            } else {
              bcrypt.compare(req.body.password,
                  user.password, (error, match) => {
                    console.log(match);

                    if (error) res.status(500).json(error);
                    else if (match) {
                      res.status(200).json({
                        message: 'success',
                        jwt_token: generateToken(user),
                      });
                    } else {
                      res.status(400).json({
                        error: 'passwords do not match',
                      });
                    }
                  });
            }
          })
          .catch((error) => {
            res.status(500).json(error);
          });
    }
  });
});

router.post('/register', (req, res) => {
  if (req.body) {
    if (req.body.email && req.body.password && req.body.role) {
      Shipper.findOne({email: req.body.email}).then((user)=>{
        if (user) {
          res.status(400)
              .json({error: 'User with that email is already exists'});
        } else {
          Driver.findOne({email: req.body.email})
              .then((user) => {
                if (user) {
                  res.status(400)
                      .json(
                          {error: 'User with that email is already exists'});
                } else {
                  console.log('userRole', user);
                  bcrypt.hash(req.body.password, rounds, (error, hash) => {
                    if (error) res.status(500).json(error);
                    else {
                      if (req.body.role === 'DRIVER') {
                        const newUser = new Driver({
                          email: req.body.email,
                          password: hash,
                          role: 'DRIVER',
                        });
                        newUser.save()
                            .then((user) => {
                              res.status(200).json({
                                message: 'success',
                                jwt_token: generateToken(user),
                              });
                            })
                            .catch((error) => {
                              res.status(500).json(error);
                            });
                      } else if (req.body.role === 'SHIPPER') {
                        const newUser = new Shipper({
                          email: req.body.email,
                          password: hash, role: 'SHIPPER',
                        });
                        newUser.save()
                            .then((user) => {
                              res.status(200).json({
                                message: 'success',
                                jwt_token: generateToken(user),
                              });
                            })
                            .catch((error) => {
                              res.status(500).json(error);
                            });
                      } else {
                        res.status(400).json({message: 'Invalid role'});
                      }
                    }
                  });
                }
              });
        }
      });
    } else {
      res.status(400).json({message: 'Invaid body in request'});
    }
  } else {
    res.status(400).json({message: 'Body required in request'});
  }
});

router.get('/jwt-test', middleware.verify, (req, res) => {
  console.log('user', req.user.data.role);

  res.status(200).json(req.user);
});
/**
 *
 * @param {*} user
 * @return {string}
 */
function generateToken(user) {
  return jwt.sign({data: user}, tokenSecret, {expiresIn: '24h'});
}

module.exports = router;
