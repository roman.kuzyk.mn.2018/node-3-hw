allowedTypes = [
  {
    type: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
];

