
const mongoose = require('mongoose');

const model = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },

  assigned_to: {
    type: String,

  },
  status: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
  width: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },

});

module.exports = mongoose.model('Truck', model);
