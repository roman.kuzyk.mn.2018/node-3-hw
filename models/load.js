const mongoose = require('mongoose');

const model = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  created_by: {
    type: String,

  },
  assigned_to: {
    type: String,
  },

  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,

  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,

  },
  payload: {
    type: Number,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  logs: {
    type: Array,
  },
});

module.exports = mongoose.model('Load', model);
