const mongoose = require('mongoose');

const model = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  trucks: {
    type: Array,

  },
  assignedTruck: {
    type: String,


  },
  assignedLoad: {
    type: String,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Driver', model);
